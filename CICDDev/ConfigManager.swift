//
//  ConfigManager.swift
//  Tickets
//
//  Created by Admin on 23/12/2023.
//

import Foundation

enum PlistKeys : String{
    case BASE_URL
    case GOOGLE_API_KEY
    case PRODUCT_BUNDLE_IDENTIFIER
    case App_Version = "CFBundleShortVersionString"
    case Build_Number = "CFBundleVersion"
}

class ConfigManager{
    static var shared : ConfigManager = ConfigManager()
    
    private static let infoPlist : [String : Any] = {
        guard let infoPlist = Bundle.main.infoDictionary else{
            fatalError("Cannot find the plist")
        }
        return infoPlist
    }()
    
    static let baseURL : String = {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

        guard let url = ConfigManager.infoPlist[PlistKeys.BASE_URL.rawValue] as? String else{
            fatalError("Cannot access the base url")
        }
        
        return url
    }()
    
    static let googleAPIKey : String = {
        guard let googleAPIKey = ConfigManager.infoPlist[PlistKeys.GOOGLE_API_KEY.rawValue] as? String else{
            fatalError("Cannot access the base GoogleAPIKey")
        }
        
        return googleAPIKey
    }()
    
    static let bundleIndentitifer : String = {
        guard let bundleIndentitifer = ConfigManager.infoPlist[PlistKeys.PRODUCT_BUNDLE_IDENTIFIER.rawValue] as? String else{
            fatalError("Cannot access the base budleIndentitifer")
        }
        
        return bundleIndentitifer
    }()
    
    static let appVersion : String = {
        guard let appVersion = ConfigManager.infoPlist[PlistKeys.App_Version.rawValue] as? String else{
            fatalError("Cannot access the base budleIndentitifer")
        }
        
        return appVersion
    }()
    static let buildNumber : String = {
        guard let buildNumber = ConfigManager.infoPlist[PlistKeys.Build_Number.rawValue] as? String else{
            fatalError("Cannot access the base budleIndentitifer")
        }
        
        return buildNumber
    }()
}
